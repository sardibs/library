<?php 
if(!function_exists('enc')){
	function enc($text){
		$CI = & get_instance();
		$enc = $CI->encryption->encrypt($text);
		$enc = strtr(
            $enc,
            array(
            	'+' => '.',
                '=' => '-',
                '/' => '~'
            )
        );
		return $enc;
	}
}

if(!function_exists('dec')){
	function dec($cipher_text){
		$CI = & get_instance();
		$cipher_text = strtr(
            $cipher_text,
            array(
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        );
		return $CI->encryption->decrypt($cipher_text);
	}
}

if(!function_exists('check_role')){
	function check_role($id_level, $id_role){
		$CI = & get_instance();
		$allow = $CI->db->select('COUNT(*) as total')
				->from(TB_LEVEL_ROLE.' a')
				->join(TB_ROLE.' b', 'b.id = a.id_role')
				->where(['a.id_level'=>$id_level, 'a.id_role'=>$id_role, 'b.is_active'=>1])
				->get()
				->row();

		if($allow->total > 0)
			return true;
		die('Mohon maaf, anda tidak memiliki akses ke halaman ini !');
	}
}

if(!function_exists('record_activity')){
	function record_activity($activity='', $username=''){
		$ipaddress = "";
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';

		$data = array(
			'username'=>$username,
			'activity'=>$activity,
			'ip_address'=>$ipaddress,
			'agent'=>$_SERVER["HTTP_USER_AGENT"]
		);

		$CI = &get_instance();
		$CI->db->insert('logs', $data);
	}
}