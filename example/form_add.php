<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="content">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">User</a></li>
              <li class="breadcrumb-item active">Tambah User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" id="app">
      <div class="container-fluid">
        
        <div class="card card-success">
          <div class="card-header">
            <div class="card-title">Tambah Baru</div>
          </div>
          <div class="card-body">
            <form class="horiontal">
              <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="nama" v-model='nama'/>
                </div>
              </div>

              <div class="form-group row">
                <label for="username" class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="username" v-model='username'/>
                </div>
              </div>

              <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-4">
                  <input type="password" class="form-control" id="password" v-model='password'/>
                </div>
              </div>

              <div class="form-group row">
                <label for="level" class="col-sm-2 col-form-label">Level</label>
                <div class="col-sm-4">
                  <?php echo combo_level("level", "level")?>
                </div>
              </div>
            </form>
          </div>
          <div id="error"></div>
          <div class="card-footer">
            <button class="btn btn-success" v-on:click='save'>Simpan</button>
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <script type="application/javascript">
      $(document).ready(function() {
          $('#level').select2();
      });
      var app = new Vue({
        el:'#app',
        data:{
          username:'',
          password:'',
          nama:'',
          level:''
        },
        methods:{
          save(){
            let form = new FormData()
            this.level = $('#level option:selected').val();
            form.append('nama', this.nama)
            form.append('username', this.username)
            form.append('password', this.password)
            form.append('level', this.level)

            PostX('user/save_new', form)
            .then(result=>{
              if(result.error_code === 0)
                swal(result.msg, {icon:'success'})
              else
                $('#error').html(result.msg);
            })
          }
        },
      });
      
    </script>
    <!-- /.content -->
  </div>