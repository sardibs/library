<?php
if(!function_exists("time_ago")){
	date_default_timezone_set("Asia/Jakarta");
	function time_ago($time, $tense='ago') {
		// declaring periods as static function var for future use
		static $periods = array('tahun', 'bulan', 'hari', 'jam', 'menit', 'detik');

		// checking time format
		if(!(strtotime($time)>0)) {
			return trigger_error("Wrong time format: '$time'", E_USER_ERROR);
		}

		// getting diff between now and time
		$now  = new DateTime('now');
		$time = new DateTime($time);
		$diff = $now->diff($time)->format('%y %m %d %h %i %s');
		// combining diff with periods
		$diff = explode(' ', $diff);
		$diff = array_combine($periods, $diff);
		// filtering zero periods from diff
		$diff = array_filter($diff);
		// getting first period and value
		$period = key($diff);
		$value  = current($diff);

		// if input time was equal now, value will be 0, so checking it
		if(!$value) {
			$period = 'detik';
			$value  = 0;
		} else {
			// converting days to weeks
			if($period=='day' && $value>=7) {
				$period = 'minggu';
				$value  = floor($value/7);
			}
			// adding 's' to period for human readability
			if($value>1) {
				$period .= '';
			}
		}

		// returning timeago
		return "$value $period $tense";
	}
}

if(!function_exists("tanggal")){
	function tanggal($tanggal, $cetak_hari = false){
        if(empty($tanggal) || $tanggal == '0000-00-00' || $tanggal == null || $tanggal == 'null' || 
            $tanggal == 'NaN-NaN-NaN')
            return "";
		$hari = array ( 1 =>    'Senin',
					'Selasa',
					'Rabu',
					'Kamis',
					'Jumat',
					'Sabtu',
					'Minggu'
		);

		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split 	  = explode('-', $tanggal);
		$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

		if ($cetak_hari) {
			$num = date('N', strtotime($tanggal));
			return $hari[$num] . ', ' . $tgl_indo;
		}
		return $tgl_indo;
	}
}

if ( ! function_exists('bulan'))
{
    function bulan($bln)
    {
        switch ($bln)
        {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
}

if ( ! function_exists('hari'))
{
    function hari($hari)
    {
        switch ($hari)
        {
            case 1:
                return "Senin";
                break;
            case 2:
                return "Selasa";
                break;
            case 3:
                return "Rabu";
                break;
            case 4:
                return "Kamis";
                break;
            case 5:
                return "Jum'at";
                break;
            case 6:
                return "Sabtu";
                break;
            case 7:
                return "Minggu";
                break;
        }
    }
}

if( ! function_exists('tgl_indo_timestamp')){
 
    function tgl_indo_timestamp($tgl)
    {
        $inttime=date('Y-m-d H:i:s',$tgl); //mengubah format menjadi tanggal biasa
        $tglBaru=explode(" ",$inttime); //memecah berdasarkan spaasi
         
        $tglBaru1=$tglBaru[0]; //mendapatkan variabel format yyyy-mm-dd
        $tglBaru2=$tglBaru[1]; //mendapatkan fotmat hh:ii:ss
        $tglBarua=explode("-",$tglBaru1); //lalu memecah variabel berdasarkan -
     
        $tgl=$tglBarua[2];
        $bln=$tglBarua[1];
        $thn=$tglBarua[0];
     
        $bln=bulan($bln); //mengganti bulan angka menjadi text dari fungsi bulan
        $ubahTanggal="$tgl $bln $thn, Pukul $tglBaru2 "; //hasil akhir tanggal
     
        return $ubahTanggal;
    }
}
?>