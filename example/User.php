<?php

class User extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('UsersModel', 'model');
	}

	public function index(){
		$keywords = $this->input->post('keywords');
		$data['keywords'] = $keywords;
		if($this->uri->segment(3))
			$from = $this->uri->segment(3);
		else
			$from = 0;

		$config = [
			'target'=>'#content',
			'base_url'=>'user/index',
			'per_page'=>10,
		];
		$list = $this->model->list($keywords, $config['per_page'], $from);
		$config['total_rows'] = $list['total'];
		$this->ajax_pagination->initialize($config);
		$data['list'] = $list['data'];
		render('user/index', $data);
	}

	public function add_new(){
		render('user/form_add');
	}

	public function save_new(){
		$rules = [
			[
				'field'=>'nama',
				'rules'=>'required',
				'errors'=>[
					'required'=>'Nama tidak boleh kosong !'
				]
			],
			[
				'field'=>'username',
				'rules'=>'required',
				'errors'=>[
					'required'=>'Username tidak boleh kosong !'
				]
			],
			[
				'field'=>'password',
				'rules'=>'required',
				'errors'=>[
					'required'=>'Password tidak boleh kosong !'
				]
			],
			[
				'field'=>'level',
				'rules'=>'required',
				'errors'=>[
					'required'=>'Level pengguna tidak boleh kosong'
				]
			]
		];

		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()){
			$data = [
				'username'=>$this->input->post('username'),
				'password'=>$this->input->post('password'),
				'nama'=>$this->input->post('nama'),
				'level'=>$this->input->post('level'),
				'is_active'=>1
			];
			if($this->model->save_new($data)){					
				$response = [
					'error_code'=>0,
					'msg'=>'Berhasil menambahkan user baru'
				];
			}
			else{
				$response = [
					'error_code'=>1,
					'msg'=>'Gagal menambahkan user baru'
				];
			}
		}
		else{
			$response = [
				'error_code'=>1,
				'msg'=>validation_errors()
			];
		}

		echo json_encode($response);
	}
}