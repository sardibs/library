<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="content">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" id="app">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        
        <!-- /.row -->
        <!-- Main row -->
        <div class="card card-primary card-outline">
          <div class="card-header">
            <button class="btn btn-primary" v-on:click='add_new'><i class='fa fa-plus-circle'></i> Baru</button>
            <div class="form-group" style="margin-top: 5px">
              <input type="text" v-on:keyup.enter="cari(false)" name="table_search" class="form-control float-right" placeholder="Search" v-model='keywords' ref="keywords">
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <table class="table">
                <thead>
                  <th>#</th>
                  <th>Aksi</th>
                  <th>Username</th>
                  <th>Nama</th>
                  <th>Level</th>
                </thead>
                <tbody>
                  <?php 
                    $no = 1;
                    foreach ($list as $row) {
                      echo "<tr>";
                      echo "<td>".($no++)."</td>";
                      echo "<td></td>";
                      echo "<td>$row->username</td>";
                      echo "<td>$row->nama</td>";
                      echo "<td>$row->nama_level</td>";
                      echo "</tr>";
                    }
                  ?>
                </tbody>
              </table>
              <hr/>
              <?php 
                echo $this->ajax_pagination->create_links();
              ?>
            </div>
          </div>
        </div>
        <!-- /.row (main row) -->
        
      </div><!-- /.container-fluid -->
    </section>
    <script type="application/javascript">
      var app = new Vue({
        el:'#app',
        data:{
          keywords:'<?php echo $keywords?>',
        },
        methods:{
          cari(is_refresh){
            if(!is_refresh)
              search('#content', 'user/index', this.keywords);
            else
              search('#content', 'user/index', null);
          },
          add_new(){
            open_menu('add_user','', '<?php echo base_url()?>index.php/user/add_new');
          }
        },
        created(){
          let key = '<?php echo $keywords?>';
          if(key !== '')
            this.$nextTick(()=>this.$refs.keywords.focus())
        }
      })
    </script>
    <!-- /.content -->
  </div>