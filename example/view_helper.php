<?php
if(!function_exists('render')){
	function render($page, $data=null){
		$CI = &get_instance();

		$is_ajax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'fetch') || $CI->input->is_ajax_request();
		if($is_ajax)
			$CI->load->view($page, $data);
		else{
			$CI->load->view('layout/header');
			$CI->load->view('layout/menu');
			$CI->load->view($page, $data);
			$CI->load->view('layout/footer');
		}
	}
}

if(!function_exists('combo_level')){
	function combo_level($model, $id){
		$CI = &get_instance();
		$level = $CI->db->select('*')
				->from(TB_LEVEL)
				->get()
				->result();

		$html = "<select v-model='$model' id='$id' class='form-control'>";
		$html.= "<option value=''>Pilih Level</option>";
		foreach ($level as $row) {
			$html.="<option value='$row->id'>$row->nama_level</option>";
		}

		$html .= "</select>";
		return $html;
	}
}