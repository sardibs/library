<?php

class Auth extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('AuthModel', 'model');
	}

	public function index(){
		$this->load->view('auth/form_login');
	}

	public function do_login(){
		$rules = [
			[
				'field'=>'username',
				'rules'=>'required',
				'errors'=>[
					'required'=>'Username tidak boleh kosong !'
				]
			],
			[
				'field'=>'password',
				'rules'=>'required',
				'errors'=>[
					'required'=>'Password tidak boleh kosong !'
				]
			]
		];

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() != FALSE){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user_data = $this->model->get_one($username);
			if(!empty($user_data)){
				if($this->model->validate($username, md5($password))){
					$session = [
						'is_login'=>true,
						'username'=>$username,
						'nama'=>$user_data->nama,
						'level'=>$user_data->level
					];

					$this->session->set_userdata($session);
					redirect('home');
				}
				else{
					$data['error'] = "Username dan Password tidak cocok !";
					$this->load->view('auth/form_login', $data);
				}
			}
			else{				
				$data['error'] = "Pengguna tidak terdaftar di sistem";
				$this->load->view('auth/form_login', $data);
			}
		}
		else{
			$data['error'] = validation_errors();
			$this->load->view('auth/form_login', $data);
		}
	}

	public function do_logout(){
		$this->session->sess_destroy();
		redirect('auth');
	}
}