<?php

require_once(APPPATH."libraries/fpdf/fpdf.php");
class TPDF extends fpdf
{
	var $widths;
	var $aligns;
	var $valigns;
	var $cut = false;
	var $h_table;
	var $border = true;
	var $cut_y = -1;
	var $hrow_plus = 0;
	var $total_height = 0;


	function SetTotalHeight($total_height){
		$this->total_height = $total_height;
	}

	function GetTotalHeight(){
		return $this->total_height;
	}

	function SetManualBorder($manual){
		$this->manual_border = $manual;
	}

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}
	
	function SetVAligns($v)
	{
		$this->valigns = $v;
	}
	
	function SetCut($c)
	{
		$this->cut = $c;
	}
	
	
	function SetHTable($h)
	{
		$this->h_table=$h;
	}
	
	function SetBorder($border){
		$this->border = $border;
	}
	
	function SetCutY($cut_y){
		$this->cut_y = $cut_y;
	}
	
	function SetHrowPlus($hrow_plus){
		$this->hrow_plus = $hrow_plus;
	}
	
	function Row($data, $next_line = true)
	{
		//Calculate the height of the row
		$nb=0;
		$total_height = 0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i], $data[$i]));
		$h=$this->h_table*$nb; //0.15
		$maxrows=floor($h/$this->FontSize);
				
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		
		$hrow=$this->FontSize;
		$is_cut = false;
		
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$rows = min($maxrows, $this->NbLines($this->widths[$i], $data[$i]));
			$dy=0;
			if($this->valigns[$i] == 'M')
				$dy=(($h-$rows*$this->FontSize)/2);
			else if($this->valigns[$i] == 'B')
				$dy=($h-$rows*$this->FontSize);
							
			if($data[$i] != 'BLANK' && $this->border)
				$this->Rect($x,$y,$w,$h,'F');
			//Print the text
			if($this->cut_y != -1 && !$is_cut){
				$y-=$this->cut_y;
				$is_cut = true;
			}
			
			$this->SetY($y+$dy);
			$this->SetX($x);
			
			if($data[$i] != 'BLANK'){
				$this->MultiCell($w, $hrow+$this->hrow_plus, $data[$i], 0,$this->aligns[$i], true);
			}
			//Put the position to the right of the cell
			if($data[$i] != 'BLANK' && $this->border)
				$this->Rect($x,$y,$w,$h);
			$total_height+=($hrow);
			$this->SetXY($x+$w,$y);
		}
		$this->SetTotalHeight($total_height);
		//Go to the next line
		if($next_line)
			$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}
?>