<?php
$type = 'TrueType';
$name = 'ArialBlack-Bold';
$desc = array('Ascent'=>728,'Descent'=>-210,'CapHeight'=>728,'Flags'=>32,'FontBBox'=>'[-45 -210 1150 896]','ItalicAngle'=>0,'StemV'=>120,'MissingWidth'=>900);
$up = -80;
$ut = 50;
$cw = array(
	chr(0)=>900,chr(1)=>900,chr(2)=>900,chr(3)=>900,chr(4)=>900,chr(5)=>900,chr(6)=>900,chr(7)=>900,chr(8)=>900,chr(9)=>900,chr(10)=>900,chr(11)=>900,chr(12)=>900,chr(13)=>900,chr(14)=>900,chr(15)=>900,chr(16)=>900,chr(17)=>900,chr(18)=>900,chr(19)=>900,chr(20)=>900,chr(21)=>900,
	chr(22)=>900,chr(23)=>900,chr(24)=>900,chr(25)=>900,chr(26)=>900,chr(27)=>900,chr(28)=>900,chr(29)=>900,chr(30)=>900,chr(31)=>900,' '=>427,'!'=>427,'"'=>504,'#'=>706,'$'=>706,'%'=>1039,'&'=>816,'\''=>340,'('=>483,')'=>483,'*'=>539,'+'=>733,
	','=>427,'-'=>483,'.'=>427,'/'=>427,'0'=>706,'1'=>706,'2'=>706,'3'=>706,'4'=>706,'5'=>706,'6'=>706,'7'=>706,'8'=>706,'9'=>706,':'=>427,';'=>427,'<'=>733,'='=>733,'>'=>733,'?'=>706,'@'=>1165,'A'=>816,
	'B'=>816,'C'=>872,'D'=>872,'E'=>816,'F'=>760,'G'=>927,'H'=>872,'I'=>427,'J'=>650,'K'=>816,'L'=>706,'M'=>983,'N'=>872,'O'=>927,'P'=>816,'Q'=>927,'R'=>872,'S'=>816,'T'=>760,'U'=>872,'V'=>816,'W'=>1093,
	'X'=>816,'Y'=>816,'Z'=>760,'['=>427,'\\'=>427,']'=>427,'^'=>619,'_'=>706,'`'=>483,'a'=>706,'b'=>706,'c'=>650,'d'=>706,'e'=>706,'f'=>427,'g'=>706,'h'=>706,'i'=>372,'j'=>372,'k'=>650,'l'=>372,'m'=>983,
	'n'=>706,'o'=>706,'p'=>706,'q'=>706,'r'=>483,'s'=>650,'t'=>427,'u'=>706,'v'=>650,'w'=>872,'x'=>650,'y'=>650,'z'=>650,'{'=>483,'|'=>409,'}'=>483,'~'=>733,chr(127)=>900,chr(128)=>900,chr(129)=>900,chr(130)=>372,chr(131)=>706,
	chr(132)=>483,chr(133)=>1150,chr(134)=>706,chr(135)=>706,chr(136)=>483,chr(137)=>1150,chr(138)=>816,chr(139)=>483,chr(140)=>1150,chr(141)=>900,chr(142)=>150,chr(143)=>900,chr(144)=>900,chr(145)=>372,chr(146)=>372,chr(147)=>483,chr(148)=>483,chr(149)=>500,chr(150)=>706,chr(151)=>1150,chr(152)=>483,chr(153)=>1150,
	chr(154)=>650,chr(155)=>483,chr(156)=>1093,chr(157)=>900,chr(158)=>150,chr(159)=>816,chr(160)=>427,chr(161)=>483,chr(162)=>706,chr(163)=>706,chr(164)=>706,chr(165)=>706,chr(166)=>409,chr(167)=>706,chr(168)=>483,chr(169)=>886,chr(170)=>520,chr(171)=>706,chr(172)=>733,chr(173)=>483,chr(174)=>886,chr(175)=>702,
	chr(176)=>549,chr(177)=>698,chr(178)=>483,chr(179)=>483,chr(180)=>483,chr(181)=>726,chr(182)=>687,chr(183)=>483,chr(184)=>483,chr(185)=>483,chr(186)=>515,chr(187)=>706,chr(188)=>983,chr(189)=>983,chr(190)=>983,chr(191)=>760,chr(192)=>816,chr(193)=>816,chr(194)=>816,chr(195)=>816,chr(196)=>816,chr(197)=>816,
	chr(198)=>1150,chr(199)=>872,chr(200)=>816,chr(201)=>816,chr(202)=>816,chr(203)=>816,chr(204)=>427,chr(205)=>427,chr(206)=>427,chr(207)=>427,chr(208)=>872,chr(209)=>872,chr(210)=>927,chr(211)=>927,chr(212)=>927,chr(213)=>927,chr(214)=>927,chr(215)=>733,chr(216)=>927,chr(217)=>872,chr(218)=>872,chr(219)=>872,
	chr(220)=>872,chr(221)=>816,chr(222)=>816,chr(223)=>760,chr(224)=>706,chr(225)=>706,chr(226)=>706,chr(227)=>706,chr(228)=>706,chr(229)=>706,chr(230)=>1039,chr(231)=>650,chr(232)=>706,chr(233)=>706,chr(234)=>706,chr(235)=>706,chr(236)=>427,chr(237)=>427,chr(238)=>427,chr(239)=>427,chr(240)=>706,chr(241)=>706,
	chr(242)=>706,chr(243)=>706,chr(244)=>706,chr(245)=>706,chr(246)=>706,chr(247)=>698,chr(248)=>760,chr(249)=>706,chr(250)=>706,chr(251)=>706,chr(252)=>706,chr(253)=>650,chr(254)=>706,chr(255)=>650);
$enc = 'cp1252';
$uv = array(0=>array(0,128),128=>8364,130=>8218,131=>402,132=>8222,133=>8230,134=>array(8224,2),136=>710,137=>8240,138=>352,139=>8249,140=>338,142=>381,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),152=>732,153=>8482,154=>353,155=>8250,156=>339,158=>382,159=>376,160=>array(160,96));
$file = 'Arial Black.z';
$originalsize = 36168;
$subsetted = true;
?>
