function PostX(to, formData, type='json'){
	return new Promise((resolve, reject)=>{
		fetch(BaseUrl+''+to, {method:'POST', body:formData, headers:{
        		'X-Requested-With':'fetch'
        }})
		.then(function(response){
			if(type == 'json')
				return response.json();
			else if(type='text')
				return response.text();
		})
		.then((res)=>{
			resolve(res)
		})
		.catch((error)=>{
			reject(error)
		})
	});
}

function GetX(to, type='json'){
    return new Promise((resolve, reject) => {
        fetch(to,{
        	headers:{
        		'X-Requested-With':'fetch'
        	}
        })
        .then(function(response){
			if(type == 'json')
				return response.json();
			else if(type='text')
				return response.text();
		})
        .then((res) => {
                resolve(res)
        })
        .catch((error) => {
        	reject(error)
      	})
    })
}